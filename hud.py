from config import *


class InfoBar:
    """
    HUD，Head up display用于显示当前血量，经验值，经验等级
    """
    def __init__(self, snake):
        self.snake = snake
        self.hp_lag = 300
        self.total_exp_num = None
        self.current_exp_num = None
        self.current_hp_num = None
        self.level_text = None
        self.update()

    # 根据蛇来更新当前HUD数值
    def update(self):
        self.total_exp_num = chat_font.render(str(100), False, (255, 255, 255))
        self.current_exp_num = chat_font.render(str(self.snake.current_exp), False, (255, 255, 255))
        self.current_hp_num = chat_font.render(str(self.snake.hp), False, (255, 255, 255))
        self.level_text = chat_font.render("Lv." + str(self.snake.level), False, (255, 255, 255))
        if self.hp_lag > self.snake.hp*3:
            self.hp_lag -= 2
        else:
            self.hp_lag = self.snake.hp*3

    # 将当前数值绘制出来
    def draw(self):
        screen.blit(self.level_text, (80, 15))
        # 绘制总经验条数值
        screen.blit(self.total_exp_num, (160, 40))
        # 绘制当前经验条数值
        screen.blit(self.current_exp_num, (5, 40))
        # 绘制当前血量信息
        screen.blit(self.current_hp_num, (90, 365))
        screen.blit(texture_lib["exp_bar"], (50, 50), pygame.Rect(0, 10, self.snake.current_exp, 10))
        screen.blit(texture_lib["exp_bar"], (50, 50), pygame.Rect(0, 0, 100, 10))
        screen.fill(-1, pygame.Rect(150, 370, self.hp_lag, 20))
        screen.blit(texture_lib["hp_bar"], (150, 370),
                    pygame.Rect(300 - self.snake.hp * 3, 20, self.snake.hp * 3, 20))
        screen.blit(texture_lib["hp_bar"], (150, 370), pygame.Rect(0, 0, 300, 20))