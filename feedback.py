import pygame
from pygame.locals import *
import sys
import tkinter as tk
from tkinter import filedialog
import os

pygame.init()
root = tk.Tk()
root.withdraw()  # 隐藏 Tkinter 窗口

class FeedbackForm:
    def __init__(self):
        self.screen_width, self.screen_height = 800, 600
        self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        pygame.display.set_caption("Feedback Form")

        self.font = pygame.font.Font(None, 36)
        self.user_name = ""
        self.problem_description = ""
        self.screenshot_path = None

        self.user_name_input_active = False
        self.problem_description_input_active = False

    def run(self):
        clock = pygame.time.Clock()
        running = True

        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
                elif event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        running = False
                    elif event.key == K_RETURN:
                        self.handle_enter_key()
                    elif event.key == K_TAB:
                        self.toggle_input_active()
                    elif self.user_name_input_active or self.problem_description_input_active:
                        self.handle_text_input(event)
                elif event.type == MOUSEBUTTONDOWN:
                    if self.is_submit_button_clicked(event.pos):
                        self.submit_feedback()
                    elif self.is_upload_button_clicked(event.pos):
                        self.upload_screenshot()

            self.draw()
            pygame.display.flip()
            clock.tick(30)

        pygame.quit()
        sys.exit()

    def draw(self):
        self.screen.fill((255, 255, 255))

        user_name_text = self.font.render("User Name: " + self.user_name, True, (0, 0, 0))
        self.screen.blit(user_name_text, (50, 50))

        problem_description_text = self.font.render("Problem Description: ", True, (0, 0, 0))
        self.screen.blit(problem_description_text, (50, 100))

        # 创建一个矩形来表示多行文本框
        problem_description_rect = pygame.Rect(300, 100, 400, 200)
        pygame.draw.rect(self.screen, (255, 255, 255), problem_description_rect)  # 多行文本框背景
        pygame.draw.rect(self.screen, (0, 0, 0), problem_description_rect, 2)  # 多行文本框边框

        # 渲染问题描述文本
        wrapped_text = self.wrap_text(self.problem_description, 400, self.font)
        for i, line in enumerate(wrapped_text):
            text_surface = self.font.render(line, True, (0, 0, 0))
            self.screen.blit(text_surface, (310, 110 + i * (text_surface.get_height() + 5)))

        upload_button_text = self.font.render("Upload Screenshot", True, (255, 255, 255))
        pygame.draw.rect(self.screen, (0, 128, 0), (50, 320, upload_button_text.get_width() + 10, 40))
        self.screen.blit(upload_button_text, (55, 325))

        submit_button_text = self.font.render("Submit Feedback", True, (255, 255, 255))
        pygame.draw.rect(self.screen, (0, 128, 255), (50, 380, submit_button_text.get_width() + 10, 40))
        self.screen.blit(submit_button_text, (55, 385))

        if self.user_name_input_active:
            pygame.draw.rect(self.screen, (0, 0, 0), (50, 50, 200, 40), 2)

    def is_submit_button_clicked(self, pos):
        submit_button_rect = pygame.Rect(50, 380, 200, 40)
        return submit_button_rect.collidepoint(pos)

    def is_upload_button_clicked(self, pos):
        upload_button_rect = pygame.Rect(50, 325, 200, 40)
        return upload_button_rect.collidepoint(pos)

    def toggle_input_active(self):
        self.user_name_input_active = not self.user_name_input_active
        self.problem_description_input_active = not self.problem_description_input_active

    def handle_text_input(self, event):
        if self.user_name_input_active:
            if event.key == K_BACKSPACE:
                self.user_name = self.user_name[:-1]
            elif event.key in (K_a, K_b, K_c, K_d, K_e, K_f, K_g, K_h, K_i, K_j, K_k, K_l, K_m, K_n, K_o, K_p, K_q, K_r, K_s, K_t, K_u, K_v, K_w, K_x, K_y, K_z):
                self.user_name += event.unicode
        elif self.problem_description_input_active:
            if event.key == K_BACKSPACE:
                self.problem_description = self.problem_description[:-1]
            elif event.key in (K_a, K_b, K_c, K_d, K_e, K_f, K_g, K_h, K_i, K_j, K_k, K_l, K_m, K_n, K_o, K_p, K_q, K_r, K_s, K_t, K_u, K_v, K_w, K_x, K_y, K_z,
                               K_SPACE, K_COMMA, K_PERIOD, K_QUESTION, K_EXCLAIM, K_MINUS, K_UNDERSCORE, K_0, K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9, K_RETURN):
                self.problem_description += event.unicode

    def handle_enter_key(self):
        if self.user_name_input_active:
            self.user_name_input_active = False
            self.problem_description_input_active = True
        elif self.problem_description_input_active:
            self.user_name_input_active = False
            self.problem_description_input_active = False

    def submit_feedback(self):
        feedback_folder = "feedback"
        if not os.path.exists(feedback_folder):
            os.makedirs(feedback_folder)

        feedback_file_path = os.path.join(feedback_folder, "feedback.txt")
        with open(feedback_file_path, "a") as f:
            f.write("User Name: {}\nProblem Description: {}\nScreenshot Path: {}\n\n".format(
                self.user_name, self.problem_description, self.screenshot_path))
        print("Feedback submitted successfully!")

    def upload_screenshot(self):
        # 使用文件对话框让用户选择截图文件
        file_path = filedialog.askopenfilename(title="Select Screenshot", filetypes=[("Image files", "*.png;*.jpg;*.jpeg")])
        if file_path:
            self.screenshot_path = file_path
            print("Screenshot uploaded:", self.screenshot_path)

    @staticmethod
    def wrap_text(text, width, font):
        """
        自动换行处理
        """
        words = text.split(' ')
        wrapped_text = []
        line = []
        current_width = 0

        for word in words:
            word_surface = font.render(word, True, (0, 0, 0))
            word_width, word_height = word_surface.get_size()

            if current_width + word_width <= width:
                line.append(word)
                current_width += word_width
            else:
                wrapped_text.append(' '.join(line))
                line = [word]
                current_width = word_width

        wrapped_text.append(' '.join(line))
        return wrapped_text

if __name__ == "__main__":
    feedback_form = FeedbackForm()
    feedback_form.run()
